import {memo, useCallback, useState} from "react";
import sortBy from 'lodash.sortby'
import {Card, Space} from 'antd';

import Products from "./Products";
import SortBySelect from "./components/SelectSortBy";
import SelectFilterBy from "./components/SelectFilterBy";

const ProductList = ({selectedData, setSelectedData, dataList, preparations}) => {

    const [filteredData, setFilteredData] = useState(dataList);

    const filterDataBy = useCallback((filter) => {
        if (!filter) {
            setFilteredData(dataList);
        } else {
            setFilteredData(dataList.filter(data => data.preparation === filter));
        }
    }, [dataList])

    const sortDataBy = useCallback((param) => {
        const sortedData = sortBy(filteredData, [param]);
        setFilteredData(sortedData)
    }, [filteredData])

    return (
        <Card
            title="Coffee List"
            bordered={false}
            style={{width: "100%"}}
            extra={(
                <Space>
                    <SelectFilterBy filterDataBy={filterDataBy} options={preparations}/>
                    <SortBySelect sortDataBy={sortDataBy}/>
                </Space>
            )}
        >
            <Products
                selectedProduct={selectedData}
                setProduct={setSelectedData}
                data={filteredData}
            />
        </Card>
    )
}

export default memo(ProductList);
