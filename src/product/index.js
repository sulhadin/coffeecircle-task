import ProductList from "./ProductList";
import Detail from "./Detail";
import {Card} from 'antd';
import {memo} from "react";

const Product = ({selectedData, setSelectedData, dataList, preparations}) => {
    return (
        <>
            <Detail data={selectedData}/>
            <ProductList
                preparations={preparations}
                selectedData={selectedData}
                setSelectedData={setSelectedData}
                dataList={dataList}
            />
        </>
    )
}

export default memo(Product);
