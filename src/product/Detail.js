import {memo, useState} from "react";
import {Button, Col, Descriptions, Row, message, Card} from "antd";

import Image from './components/Image'
import Variants from "./components/Variants";

const Detail = ({data}) => {
    const [selectedGrind, setSelectedGrind] = useState(data.variants.grind[0]);
    const [selectedSize, setSelectedSize] = useState(data.variants.size[0]);
    const [loading, setLoading] = useState(false);

    const onClick = () => {
        setLoading(true);

        window.addToWishlist(`${data.skuBase}-${selectedSize.code}-${selectedGrind.code}`, 1)
            .then(() => {
                message.success(`${data.name} added to the wishlist!`);
            })
            .catch(() => {
                message.error('We could not added it to the wishlist.');
            })
            .finally(() => setLoading(false));
    }

    return (
        <Card title="Coffee Detail" bordered={false} style={{width: "100%"}}>
            <Row gutter={[16, 16]}>
                <Col xs={24} md={12}>
                    <Image src={data.image}/>
                </Col>
                <Col xs={24} md={12}>
                    <Descriptions title={data.name}>
                        <Descriptions.Item label="Preparation" span={3}>{data.preparation}</Descriptions.Item>
                        <Descriptions.Item label="Price" span={3}>{data.price}€</Descriptions.Item>
                        <Descriptions.Item label="Grind" span={3}>
                            <Variants
                                data={data.variants.grind}
                                selectedVariant={selectedGrind.code}
                                setVariant={setSelectedGrind}
                            />
                        </Descriptions.Item>
                        <Descriptions.Item label="Grind" span={3}>
                            <Variants
                                data={data.variants.size}
                                selectedVariant={selectedSize.code}
                                setVariant={setSelectedSize}
                            />
                        </Descriptions.Item>
                        <Descriptions.Item span={3}>
                            <Button loading={loading} onClick={onClick}>+ Add to wishlist</Button>
                        </Descriptions.Item>
                    </Descriptions>
                </Col>
            </Row>
        </Card>
    );
}

export default memo(Detail);
