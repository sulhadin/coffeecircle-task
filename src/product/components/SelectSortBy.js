import {Select} from "antd";
import {memo} from "react";

const SelectSortBy = ({sortDataBy}) => {
    return (
        <>
            <span>Sort by: </span>
            <Select defaultValue={"name"} style={{width: 120}} onChange={sortDataBy}>
                <Select.Option value={"name"}>Name</Select.Option>
                <Select.Option value={"price"}>Price</Select.Option>
                <Select.Option value={"preparation"}>Preparation</Select.Option>
            </Select>
        </>
    );
}
export default memo(SelectSortBy)
