import {Select} from "antd";
import {memo} from "react";

const SelectFilterBy = ({filterDataBy, options}) => {
    return (
        <>
            <span>Filter by: </span>
            <Select defaultValue={null} style={{width: 120}} onChange={filterDataBy}>
                <Select.Option value={null}>All</Select.Option>
                {options.map(option => (
                    <Select.Option key={option} value={option}>{option}</Select.Option>
                ))}
            </Select>
        </>
    );
}
export default memo(SelectFilterBy)
