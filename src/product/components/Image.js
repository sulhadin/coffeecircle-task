import React, { useState } from 'react';
import { Image as AntdImage } from 'antd';

const Image = ({src}) => {
    const [visible, setVisible] = useState(false);
    return (
        <>
            <AntdImage
                preview={{ visible: false }}
                style={{width:"100%"}}
                src={src}
                onClick={() => setVisible(true)}
            />
            <div style={{ display: 'none' }}>
                <AntdImage.PreviewGroup preview={{ visible, onVisibleChange: vis => setVisible(vis) }}>
                    <AntdImage src={src} />
                </AntdImage.PreviewGroup>
            </div>
        </>
    );
};

export default Image;
