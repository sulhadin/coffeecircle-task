import {Button, Space} from "antd";

const Variants = ({data, selectedVariant, setVariant}) => {

    const getType = (code) => code === selectedVariant ? "primary" : "dashed"

    return (
        <Space>
            {
                data.map(item => (
                    <Button
                        key={item.code}
                        onClick={() => setVariant(item)}
                        type={getType(item.code)}
                    >
                        {item.label}
                    </Button>
                ))
            }</Space>
    );
}

export default Variants;
