import {memo} from "react";
import {Button, Card, List, Tag} from "antd";
import Meta from "antd/es/card/Meta";
import {FileSearchOutlined} from "@ant-design/icons";

import {scrollUp} from "../utils/scrollUp";

const Products = ({data, selectedProduct, setProduct}) => {

    const onItemClick = (item) => {
        setProduct(item)
        scrollUp();
    }

    return (
        <List
            grid={{gutter: 16, xs: 1, sm: 1, md: 2, lg: 3, xl: 4, xxl: 4}}
            dataSource={data}
            renderItem={item => (
                <List.Item>
                    <Card
                        style={{padding: 5}}
                        cover={<img alt={item.name} src={item.image}/>}
                        actions={[
                            <Button
                                icon={<FileSearchOutlined/>}
                                onClick={() => onItemClick(item)}
                                disabled={selectedProduct.skuBase === item.skuBase}
                            >
                                See Detail
                            </Button>,
                        ]}
                    >
                        <Meta
                            title={item.name}
                            description={
                                <>
                                    <Tag color="magenta">{item.preparation}</Tag>
                                    <Tag color="green">{item.price}€</Tag>
                                </>
                            }
                        />
                    </Card>
                </List.Item>
            )}
        />
    );
}

export default memo(Products);
