import ReactDOM from 'react-dom';
import App from './App';
import "./App.less"

ReactDOM.render(
  <App/>,
  document.getElementById('mounting-point'),
);
