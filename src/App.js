import Product from "./Product";
import Coffees from './data/coffees.json'
import {useMemo, useState} from "react";

const App = () => {
    const [state, setState] = useState(Coffees[0]);

    const preparations = useMemo(() => [...new Set(Coffees.map((item) => item.preparation))], [Coffees])
    return <Product
        dataList={Coffees}
        preparations={preparations}
        selectedData={state}
        setSelectedData={setState}/>
}

export default App;
